package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/bluebottle/merge-test/configuration"
	"gitlab.com/bluebottle/merge-test/misc"

	git "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport/ssh"
)

// Return public ssh key
func publicKey() *ssh.PublicKeys {
	sshPath := os.Getenv("HOME") + "/.ssh/id_rsa"
	sshKey, _ := ioutil.ReadFile(sshPath)
	publicKey, err := ssh.NewPublicKeys("git", []byte(sshKey), "")
	misc.ErrorPanic(err)
	return publicKey
} // publicKey ()

// Clone the desired repo with given name and URL. If needed a ssh key will be used.
func cloneRepo(name, url string, sshNeeded bool) (*git.Repository, error) {
	var (
		err     error
		tmpRepo *git.Repository
	)

	if sshNeeded {
		tmpRepo, err = git.PlainClone(name, false, &git.CloneOptions{
			URL:      url,
			Progress: os.Stdout,
			Auth:     publicKey(),
		})
	} else {
		tmpRepo, err = git.PlainClone(name, false, &git.CloneOptions{
			URL:      url,
			Progress: os.Stdout,
		})
	} // if

	return tmpRepo, err
} // cloneRepo ()

// Execute shell command and display output
func cmdOutput(cmd string, args ...string) {
	b, err := exec.Command(cmd, args...).Output()
	misc.ErrorPanic(err)
	fmt.Println(string(b))
} // cmdOutput ()

// Handle eclasses, filters and copying
func foundationWork(name string, v configuration.Definition) {
	// Copy eclasses to liguros
	for _, val := range v.Eclasses {
		fmt.Println("Copying eclass: " + name + "/eclass/" + val + ".eclass")
		cmdOutput("sh", "-c", "cp -fv "+name+"/eclass/"+val+".eclass liguros-repo/eclass")
	} // for

	// Filter packages, packages or ebuilds
	if len(v.Filter) > 0 {
		for _, val := range v.Filter {
			files, err := filepath.Glob(name + "/" + val)
			misc.ErrorPanic(err)

			if len(files) > 0 {
				for _, file := range files {
					fmt.Println("Removing: " + file)
					err = os.RemoveAll(file)
					misc.ErrorPanic(err)
				} // for
			} else {
				fmt.Println("Filter: " + name + "/" + val + " not found")
			} // if
		} // for
	} // if

	// Copy files to liguros
	for _, val := range v.Copyfiles {
		fmt.Println("Copying file: " + name + "/" + val)
		cmdOutput("sh", "-c", "cp -f "+name+"/"+val+" liguros-repo/"+val)
	} // for
} // foundationWork ()

// Copy package over to liguros with removing existing package at destination first
func copyPackage(pack string) {
	fmt.Println("Package: " + pack)

	// Check if source directory exists
	if _, err := os.Stat(pack); !os.IsNotExist(err) {
		// Check if directory exist in liguros, if yes => remove it
		if _, err := os.Stat("../liguros-repo/" + pack); !os.IsNotExist(err) {
			fmt.Println("Removing: ../liguros-repo/" + pack)
			err = os.RemoveAll("../liguros-repo/" + pack)
			misc.ErrorPanic(err)
		} // if

		fmt.Println("Copying: " + pack + " to ../liguros-repo/")
		cmdOutput("sh", "-c", "cp -rv "+pack+" ../liguros-repo/"+pack)
	} else {
		fmt.Println("Package " + pack + " not existing")
	} // if
} // copyPackage ()

// Copy category over to liguros with removing existing category at destination first
func copyCategory(cat string) {
	fmt.Println("Category: " + cat)
	packs, err := filepath.Glob(cat + "/*")
	misc.ErrorPanic(err)

	for _, pack := range packs {
		mode, err := os.Stat(pack)
		misc.ErrorPanic(err)

		if mode.Mode().IsDir() {
			copyPackage(pack)
		} // if
	} // for
} // copyCategory ()

// Copy repository to liguros (skipping metadata, profiles and .git)
func copyRepo(repo string) {
	fmt.Println("Copy all from " + repo + " to liguros")
	err := os.Chdir(repo)
	misc.ErrorPanic(err)
	cats, err := filepath.Glob("*")
	misc.ErrorPanic(err)

	for _, cat := range cats {
		mode, err := os.Stat(cat)
		misc.ErrorPanic(err)

		if mode.Mode().IsDir() && cat != "metadata" && cat != "profiles" && cat != ".git" {
			copyCategory(cat)
		} // if
	} // for

	err = os.Chdir("..")
	misc.ErrorPanic(err)
} // copyRepo

func main() {
	// Read config file.
	configValues := configuration.ReadProgConfig("foundation.toml", ".")

	// Clone Liguros repo first
	fmt.Println("\nCloning liguros-repo")
	ligurosRepo, err := cloneRepo("liguros-repo", "git@gitlab.com:farout/gentoo-staging.git", true)
	misc.ErrorPanic(err)
	wt, err := ligurosRepo.Worktree()
	misc.ErrorPanic(err)

	// Remove all from liguros repo
	fmt.Println("\nRemove all from liguros-repo")
	wt.Remove(".")

	// Clone gentoo staging
	fmt.Println("\nCloning gentoo staging")
	_, err = cloneRepo("gentoo-staging", configValues.GentooStage.URL, false)
	misc.ErrorPanic(err)

	// Clean up gentoo repo of unwanted stuff
	fmt.Println("\nClean gentoo staging from unwanted stuff")
	foundationWork("gentoo-staging", configValues.GentooStage)

	// Copy all the rest of gentoo repo into liguros repo (as base system)
	fmt.Println("\nMoving gentoo staging to liguros repo as base")
	cmdOutput("sh", "-c", "cp -r gentoo-staging/* liguros-repo")

	// Loop through all the defined repos and add them to the liguros repo
	for i, v := range configValues.Repo {
		fmt.Println("\nCloning repository: ", i)
		_, err = cloneRepo(i, v.URL, false)
		misc.ErrorPanic(err)
		// Do the basic work with the foundation config
		foundationWork(i, v)

		// Copy selected packages to liguros if set, otherwise copy all packages left
		if len(v.Select) > 0 {
			fmt.Println("Copy selected files from " + i + " to liguros")
			err := os.Chdir(i)
			misc.ErrorPanic(err)

			for _, w := range v.Select {
				fmt.Println("Copy: ", w)
				copyPackage(w)
			} // for

			err = os.Chdir("..")
			misc.ErrorPanic(err)
		} else {
			copyRepo(i)
		} // if
	} // for

	// Clone fixups
	fmt.Println("\nCloning fixups")
	_, err = cloneRepo("flat-fixups", configValues.Fixups.URL, false)
	misc.ErrorPanic(err)
	foundationWork("flat-fixups", configValues.Fixups)
	copyRepo("flat-fixups")
	// copy profiles over from fixups to liguros repo
	cmdOutput("sh", "-c", "cp -r flat-fixups/profiles liguros-repo/profiles")

	// Push liguros repo back to gitlab
	fmt.Println("\nAdding altered files to work tree")
	// go-git still has the problem of not adding deleted files to the tree => using workaround
	cmd := exec.Command("git", "add", ".")
	cmd.Dir = wt.Filesystem.Root()
	err = cmd.Run()
	misc.ErrorPanic(err)

	fmt.Println("\nCommitting changes to liguros repo")
	_, err = wt.Commit("Updating liguros repo", &git.CommitOptions{})
	misc.ErrorPanic(err)

	fmt.Println("\nPushing liguros repo to gitlab")
	err = ligurosRepo.Push(&git.PushOptions{
		Progress: os.Stdout,
		Auth:     publicKey(),
	})
	misc.ErrorPanic(err)
} // main ()
