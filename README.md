Go program to merge gentoo like repositories into a final repository defined by a config file (foundation.toml).

Each repository in the config file can have a few paramters:

- copyfile: List of files to be copied
- eclasses: List of eclasses to be copied
- filter: List of categories, packages or package versions to exclude from final repository
- select: List of packages to include in the final repository (if not given => all packages are used)
- URL: URL of the repository to include