//Package configuration handles the configuration functionality.
package configuration

import (
	"fmt"

	"github.com/pelletier/go-toml"
)

// Configuration contains the Liguros configuration
type Configuration struct {
	GentooStage Definition            `toml:"gentoo-staging"`
	Fixups      Definition            `toml:"flat-fixups"`
	Repo        map[string]Definition `toml:"repo"`
}

// Definition defines the internal structure
type Definition struct {
	Copyfiles []string `toml:"copyfiles,omitempty"`
	Eclasses  []string `toml:"eclasses,omitempty"`
	Filter    []string `toml:"filter,omitempty"`
	Select    []string `toml:"select,omitempty"`
	URL       string   `toml:"URL"`
}

// ReadProgConfig Reads configuration file and returns content, with default values for entities not set.
func ReadProgConfig(filename, filepath string) Configuration {
	var (
		c Configuration
	)

	tfile, err := toml.LoadFile(filename)

	if err != nil {
		panic(fmt.Errorf("Error when reading config: %v", err))
	} // if

	err = tfile.Unmarshal(&c)

	if err != nil {
		panic(fmt.Errorf("unable to decode into struct, %v", err))
	} // if

	return c
} // readProgConfig ()
